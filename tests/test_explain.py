from tabnet.classifier import TabNetClassifier
from tabnet.regressor import TabNetRegressor
from tabnet.autoencoder import TabNetAutoencoder
import tabnet_utils
import read_data

import numpy as np
import pandas as pd
import tensorflow as tf
import pytest


def test_covertype_global_explaination_works():
    train, test = read_data.read_covertype()

    _, types = tabnet_utils.convert_data_types(train)

    X_train, y_train = train.iloc[:, :-1], train.iloc[:, -1]
    X_test, y_test = test.iloc[:, :-1], test.iloc[:, -1]
    y_train = y_train - 1
    y_test = y_test - 1

    columns = [tabnet_utils.get_feature(f, types[k]) for k, f in X_train.iteritems()]
    train, test = (
        tabnet_utils.df_to_dataset(X_train, y_train),
        tabnet_utils.df_to_dataset(X_test, y_test),
    )

    m = TabNetClassifier(
        feature_columns=columns,
        num_features=54,
        num_outputs=7,
        feature_dim=64,
        output_dim=64,
        num_decision_steps=5,
        relaxation_factor=1.5,
        sparsity_coefficient=0.0001,
        batch_momentum=0.9,
        classifier_activation=tf.keras.activations.softmax,
    )

    m.compile(
        tf.keras.optimizers.Adam(0.01),
        loss=tf.keras.losses.SparseCategoricalCrossentropy(),
        metrics=["accuracy"],
    )
    m.fit(train, epochs=2, validation_data=test)

    global_test = m.explain(X_test).numpy()


def test_covertype_steps_explaination_works():
    train, test = read_data.read_covertype()

    _, types = tabnet_utils.convert_data_types(train)

    X_train, y_train = train.iloc[:, :-1], train.iloc[:, -1]
    X_test, y_test = test.iloc[:, :-1], test.iloc[:, -1]
    y_train = y_train - 1
    y_test = y_test - 1

    columns = [tabnet_utils.get_feature(f, types[k]) for k, f in X_train.iteritems()]
    train, test = (
        tabnet_utils.df_to_dataset(X_train, y_train),
        tabnet_utils.df_to_dataset(X_test, y_test),
    )

    m = TabNetClassifier(
        feature_columns=columns,
        num_features=54,
        num_outputs=7,
        feature_dim=64,
        output_dim=64,
        num_decision_steps=5,
        relaxation_factor=1.5,
        sparsity_coefficient=0.0001,
        batch_momentum=0.9,
        classifier_activation=tf.keras.activations.softmax,
    )

    m.compile(
        tf.keras.optimizers.Adam(0.01),
        loss=tf.keras.losses.SparseCategoricalCrossentropy(),
        metrics=["accuracy"],
    )
    m.fit(train, epochs=2, validation_data=test)

    steps_test = m.explain_steps(X_test)


def test_sarcos_global_explaination_works():
    train, test = read_data.read_sarcos()

    train, types = tabnet_utils.convert_data_types(train)
    test, _ = tabnet_utils.convert_data_types(test)

    X_train, y_train = train.iloc[:, :-1], train.iloc[:, -1]
    X_test, y_test = test.iloc[:, :-1], test.iloc[:, -1]

    columns = [tabnet_utils.get_feature(f, types[k]) for k, f in X_train.iteritems()]
    train, test = (
        tabnet_utils.df_to_dataset(X_train, y_train),
        tabnet_utils.df_to_dataset(X_test, y_test),
    )

    m = TabNetRegressor(
        feature_columns=columns,
        num_features=21,
        num_outputs=1,
        feature_dim=16,
        output_dim=16,
        num_decision_steps=3,
        relaxation_factor=1.5,
        sparsity_coefficient=0.0001,
        batch_momentum=0.9,
    )

    m.compile(tf.keras.optimizers.Adam(0.01), loss=tf.keras.losses.MeanSquaredError())
    m.fit(train, epochs=2, validation_data=test)

    global_test = m.explain(X_test).numpy()


def test_sarcos_steps_explaination_works():
    train, test = read_data.read_sarcos()

    train, types = tabnet_utils.convert_data_types(train)
    test, _ = tabnet_utils.convert_data_types(test)

    X_train, y_train = train.iloc[:, :-1], train.iloc[:, -1]
    X_test, y_test = test.iloc[:, :-1], test.iloc[:, -1]

    columns = [tabnet_utils.get_feature(f, types[k]) for k, f in X_train.iteritems()]
    train, test = (
        tabnet_utils.df_to_dataset(X_train, y_train),
        tabnet_utils.df_to_dataset(X_test, y_test),
    )

    m = TabNetRegressor(
        feature_columns=columns,
        num_features=21,
        num_outputs=1,
        feature_dim=16,
        output_dim=16,
        num_decision_steps=3,
        relaxation_factor=1.5,
        sparsity_coefficient=0.0001,
        batch_momentum=0.9,
    )

    m.compile(tf.keras.optimizers.Adam(0.01), loss=tf.keras.losses.MeanSquaredError())
    m.fit(train, epochs=2, validation_data=test)

    steps_test = m.explain_steps(X_test)


def test_autoencoder_global_explaination_works():
    train, test = read_data.read_sarcos()

    train, types = tabnet_utils.convert_data_types(train)
    test, _ = tabnet_utils.convert_data_types(test)

    X_train = train.iloc[:, :-1]
    X_test = test.iloc[:, :-1]

    columns = [tabnet_utils.get_feature(f, types[k]) for k, f in X_train.iteritems()]
    train, test = (
        tabnet_utils.df_to_dataset(X_train, X_train),
        tabnet_utils.df_to_dataset(X_test, X_test),
    )

    ae = TabNetAutoencoder(
        feature_columns=columns,
        num_features=21,
        num_outputs=1,
        feature_dim=8,
        output_dim=8,
        num_decision_steps=3,
        num_decision_steps_decoder=5,
        relaxation_factor=1.2,
        sparsity_coefficient=0.0001,
        batch_momentum=0.9,
    )

    ae.compile(tf.keras.optimizers.Adam(0.01))
    ae.fit(train, epochs=2, validation_data=test)

    global_test = ae.explain(X_test).numpy()


def test_autoencoder_steps_explaination_works():
    train, test = read_data.read_sarcos()

    train, types = tabnet_utils.convert_data_types(train)
    test, _ = tabnet_utils.convert_data_types(test)

    X_train = train.iloc[:, :-1]
    X_test = test.iloc[:, :-1]

    columns = [tabnet_utils.get_feature(f, types[k]) for k, f in X_train.iteritems()]
    train, test = (
        tabnet_utils.df_to_dataset(X_train, X_train),
        tabnet_utils.df_to_dataset(X_test, X_test),
    )

    ae = TabNetAutoencoder(
        feature_columns=columns,
        num_features=21,
        num_outputs=1,
        feature_dim=8,
        output_dim=8,
        num_decision_steps=3,
        num_decision_steps_decoder=5,
        relaxation_factor=1.2,
        sparsity_coefficient=0.0001,
        batch_momentum=0.9,
    )

    ae.compile(tf.keras.optimizers.Adam(0.01))
    ae.fit(train, epochs=2, validation_data=test)

    steps_test = ae.explain_steps(X_test)