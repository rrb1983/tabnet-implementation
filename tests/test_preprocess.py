import tabnet_utils

from hypothesis import given, assume, settings
from hypothesis import assume
import hypothesis.strategies as st
import hypothesis.extra.pandas as hpd

import numpy as np
import pandas as pd
import tensorflow as tf
import pytest


@given(
    hpd.data_frames(
        [
            hpd.column("A", dtype="int32"),
            hpd.column("B", dtype="float32"),
            hpd.column("E", dtype=str),
            hpd.column("F", dtype=bool),
        ]
    )
)
def test_get_tensorflow_dense_feature_from_data(data):
    assume(not data.empty)
    data, types = tabnet_utils.convert_data_types(data)
    columns = [tabnet_utils.get_feature(f, types[k]) for k, f in data.iteritems()]
    for i in range(data.shape[1]):
        column = data.columns[i]
        if types[column] == "int32" or types[column] == "float32":
            assert column == columns[i].key
        else:
            assert column == columns[i][0].key


@settings(deadline=None)
@given(
    hpd.data_frames(
        [
            hpd.column("A", dtype="int32"),
            hpd.column("B", dtype="float32"),
            hpd.column("E", dtype=str),
            hpd.column("F", dtype=bool),
        ]
    )
)
def test_pandas_dataframe_to_tensorflow_prefetch_dataset(y):
    assume(not y.empty)
    assume(not y.isna().any().any())
    X, types = tabnet_utils.convert_data_types(y)
    dataset = tabnet_utils.df_to_dataset(X, y)
    for column in X.columns:
        if (
            types[column] == "int32"
            or types[column] == "float32"
            or types[column] == "bool"
        ):
            assert list(np.array(list(dataset)[0][0][column])) == list(
                np.array(X[column])
            )
        else:
            assert list(
                map(lambda x: x.decode(), list(np.array(list(dataset)[0][0][column])))
            ) == list(np.array(X[column]))
