"""
TabNet module containing the auto-encoder.
"""

import pandas as pd
import tensorflow as tf

from tabnet.encoder import TabNetEncoder
from tabnet.decoder import TabNetDecoder


class TabNetAutoencoder(tf.keras.Model):
    """
    TabNetAutoencoder for self-supervised learning, composed of a TabNetEncoder and a
    TabNetDecoder. Its objective is to learn correlations between feature columns. To do so,
    it proposes the task of prediction of missing feature columns from the others. A random
    binary mask is applied to the features. The TabNetEncoder inputs those masked features
    and returns its encoded representation. The TabNetDecoder then returns the reconstructed
    features.
    Args:
        feature_columns: The Tensorflow feature columns for the dataset.
        num_features: The number of input features (i.e the number of columns for tabular data
            assuming each feature is represented with 1 dimension).
        num_outputs: Number of outputs for classification or regression.
        feature_dim (Na): Dimensionality of the hidden representation in the Dense layer of the
            TransformBlocks.
        output_dim (Nd): Dimensionality of the outputs of each decision step, which is later
            mapped to the final classification or regression output.
        num_decision_steps (N_steps): Number of sequential decision steps.
        num_decision_steps_decoder: Number of sequential decision steps for the TabNetDecoder.
        relaxation_factor (gamma): Relaxation factor that promotes the reuse of each feature at
            different decision steps. When it is 1, a feature is enforced to be used only at one
            decision step and as it increases, more flexibility is provided to use a feature at
            multiple decision steps.
        sparsity_coefficient (lambda_sparse): Strength of the sparsity regularization. Sparsity
            may provide a favorable inductive bias for convergence to higher accuracy for some
            datasets where most of the input features are redundant.
        batch_momentum: Momentum in ghost batch normalization.
        epsilon: A small number for numerical stability of the entropy calculations.
        num_shared: Number of shared TransformBlocks in the Feature transformer.
        num_stepblock: Number of decision step dependent TransformBlocks in the feature
            transformer.
        dropout_rate: Fraction of the features to drop.
    """

    def __init__(
        self,
        feature_columns: list,
        num_features: int,
        num_outputs: int,
        feature_dim: int = 64,
        output_dim: int = 64,
        num_decision_steps: int = 5,
        num_decision_steps_decoder: int = 5,
        relaxation_factor: float = 1.5,
        sparsity_coefficient: float = 1e-5,
        batch_momentum: float = 0.9,
        epsilon: float = 1e-5,
        num_shared: int = 2,
        num_stepblock: int = 2,
        dropout_rate: float = 0.2,
        **kwargs
    ):

        super(TabNetAutoencoder, self).__init__(**kwargs)

        self.input_features = tf.keras.layers.DenseFeatures(feature_columns)

        self.input_bn = tf.keras.layers.BatchNormalization(
            axis=-1, momentum=batch_momentum, name="input_bn"
        )

        self.encoder = TabNetEncoder(
            num_features=num_features,
            num_outputs=num_outputs,
            feature_dim=feature_dim,
            output_dim=output_dim,
            num_decision_steps=num_decision_steps,
            relaxation_factor=relaxation_factor,
            sparsity_coefficient=sparsity_coefficient,
            batch_momentum=batch_momentum,
            epsilon=epsilon,
            num_shared=num_shared,
            num_stepblock=num_stepblock,
            **kwargs
        )

        self.decoder = TabNetDecoder(
            num_features=num_features,
            feature_dim=feature_dim,
            num_decision_steps_decoder=num_decision_steps_decoder,
            batch_momentum=batch_momentum,
            num_shared=num_shared,
            num_stepblock=num_stepblock,
            **kwargs
        )

        self.dropout = tf.keras.layers.Dropout(dropout_rate)

    def call(
        self,
        inputs: tf.python.data.ops.dataset_ops.TensorSliceDataset,
    ) -> tf.Tensor:
        """
        Self-supervised training function.
        Args:
            inputs: PrefetchDataset of the features.
        Returns:
            Tensor of the reconstructed tabular features.
        """
        features = self.input_features(inputs)

        mask = self.dropout(tf.ones_like(features), training=True)
        mask = tf.where(mask != 0.0, 1.0, 0.0)
        masked = features * mask
        missing = features * (1 - mask)

        normalized = self.input_bn(masked, training=True)
        _, encoded, _, _ = self.encoder(
            normalized, priors_initialization=mask, training=True
        )
        reconstructed = self.decoder(encoded, training=True)

        loss = tf.reduce_sum(tf.square(missing - reconstructed * (1 - mask)))

        self.add_loss(loss)

        return tf.where(mask != 0.0, features, reconstructed)

    def transform(self, inputs) -> tf.Tensor:
        """
        Encoded representation function.
        Args:
            inputs: Dictionnary of the features.
        Returns:
            encoded: Tensor of the features encoded representation.
        """
        if isinstance(inputs, pd.DataFrame):
            inputs = dict(inputs)

        inputs = self.input_features(inputs)
        normalized = self.input_bn(inputs, training=True)

        _, encoded, _, _ = self.encoder(normalized, training=True)
        return encoded

    def explain(self, inputs) -> tf.Tensor:
        """
        Features global importances function.
        Args:
            inputs: Dictionnary of the features.
        Returns:
            importance: Tensor of the features global importances.
        """
        if isinstance(inputs, pd.DataFrame):
            inputs = dict(inputs)

        inputs = self.input_features(inputs)
        normalized = self.input_bn(inputs, training=True)

        _, _, importance, _ = self.encoder(normalized, training=True)
        return importance

    def explain_steps(self, inputs) -> list:
        """
        Features steps importances function.
        Args:
            inputs: Dictionnary of the features.
        Returns:
            steps_importances: List of tensors of the features steps importances.
        """
        if isinstance(inputs, pd.DataFrame):
            inputs = dict(inputs)

        inputs = self.input_features(inputs)
        normalized = self.input_bn(inputs, training=True)

        _, _, _, steps_importances = self.encoder(normalized, training=True)
        return steps_importances

    def fillna(self, inputs: pd.DataFrame) -> tf.Tensor:
        """
        Filling of the missing values for numerical dataset by predicting them.
        Args:
            inputs: Pandas dataframe of the features.
        Returns:
            Tensor of the features with its missing values filled.
        """
        missing = tf.dtypes.cast(tf.math.is_nan(inputs), tf.float32)
        features = tf.constant(inputs.fillna(0))

        masked = features * (1 - missing)

        _, encoded, _, _ = self.encoder(
            masked, priors_initialization=(1 - missing), training=True
        )
        reconstructed = self.decoder(encoded, training=True)

        return masked + reconstructed * missing

    def summary(self, *super_args, **super_kwargs):
        """
        TabNetEncoder and TabNetDecoder models summary function.
        """
        super(TabNetAutoencoder, self).summary(*super_args, **super_kwargs)
        self.encoder.summary(*super_args, **super_kwargs)
        self.decoder.summary(*super_args, **super_kwargs)
