"""
TabNet module containing the decoder.
"""

import tensorflow as tf

from tabnet.building_blocks import (
    SharedFeatureTransformBlock,
    StepFeatureTransformBlock,
)


class TabNetDecoder(tf.keras.Model):
    """
    TabNetDecoder for tabular features reconstruction from an encoded representation, composed
    of a Feature transformer (SharedFeatureTransformBlock + StepFeatureTransformBlock) and
    a Dense layer at each decision step. The outputs from the decision steps are summed to
    obtain the reconstructed features.
    Args:
        num_features: The number of input features (i.e the number of columns for tabular
            data assuming each feature is represented with 1 dimension).
        feature_dim (Na): Dimensionality of the hidden representation in the Dense layer of
            the TransformBlocks.
        num_decision_steps_decoder: Number of sequential decision steps for the TabNetDecoder.
        batch_momentum: Momentum in ghost batch normalization.
        num_shared: Number of shared TransformBlocks in the Feature transformer.
        num_stepblock: Number of decision step dependent TransformBlocks in the feature
            transformer.
    """

    def __init__(
        self,
        num_features: int,
        feature_dim: int = 64,
        num_decision_steps_decoder: int = 5,
        batch_momentum: float = 0.9,
        num_shared: int = 2,
        num_stepblock: int = 2,
        **kwargs,
    ):

        super(TabNetDecoder, self).__init__(**kwargs)

        self.num_features = num_features
        self.feature_dim = feature_dim

        self.num_decision_steps_decoder = num_decision_steps_decoder
        self.batch_momentum = batch_momentum
        self.num_shared = num_shared
        self.num_stepblock = num_stepblock

        self.shared_feature_transform = SharedFeatureTransformBlock(
            self.feature_dim,
            self.batch_momentum,
            self.num_shared,
            block_name="decoder_shared",
        )

        self.step_feature_transform_list = [
            StepFeatureTransformBlock(
                self.feature_dim,
                self.batch_momentum,
                self.num_stepblock,
                block_name=f"decoder_{i}",
            )
            for i in range(self.num_decision_steps_decoder)
        ]

        self.fc_list = [
            tf.keras.layers.Dense(self.num_features, use_bias=False, name=f"dense_{i}")
            for i in range(self.num_decision_steps_decoder)
        ]

    def call(self, inputs: tf.Tensor, training: bool = None) -> tf.Tensor:
        """
        Args:
            inputs: Tensor of the features encoded representation.
            training: Python boolean indicating whether the BatchNormalization layer of the
                TransformBlocks should behave in training mode or in inference mode.
        Returns:
            decoded: Tensor of the renconstructed tabular features.
        """
        decoded = 0

        for i in range(self.num_decision_steps_decoder):
            shared = self.shared_feature_transform(inputs, training=training)
            step = self.step_feature_transform_list[i](shared, training=training)
            features = self.fc_list[i](step, training=training)

            decoded += features

        return decoded
