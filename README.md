# TabNet implementation

TabNet implementation by Maxime Lecardonnel <mlecardo@hotmail.fr> in Tensorflow 2.3.1 for reality check and evaluation against XGBoost.

Paper: [Arik, Sercan O., and Tomas Pfister. "Tabnet: Attentive interpretable tabular learning." arXiv preprint arXiv:1908.07442 (2019).](https://arxiv.org/abs/1908.07442)



Released in the above research paper in 2019, TabNet's goal is to surpass existing methods of predicting tabular data by exploring deep learning for tabular data.

Claimed benefits:
- Encode diverse typed raw data into meaningful representation.
- Improving performance thanks to neural networks.
- Interpretability thanks to a mapping similar to a decision tree.
- Self-supervised learning to improve performance

TabNet prediction relies on a sequential processing in several decision steps. Each step takes as input the information from the previous step to decide which features to use by building masks. The aggregation of the processed masked features from each step builds the overall decision.

TabNet proposes self-supervised learning for tabular data for the first time. It allows the data scientist to pre-train a model on a large unlabelled dataset, then to train the model on a small labelled dataset and to still obtain good performances. TabNet self-supervised learning relies on an auto-encoder architecture. After applying a random mask to the dataset, the masked features are encoded into a representation. The hidden features are reconstructed from the encoded representation with the decoder. Thus, the auto-encoder learns the correlations between features by predicting the missing features with the others.

## Datasets

The dataset used to test this implementation and as Tutorial come from the UCI Data Science repository.
Dua, D. and Graff, C. (2019). UCI Machine Learning Repository [http://archive.ics.uci.edu/ml]. Irvine, CA: University of California, School of Information and Computer Science.

## Implementation

TabNet implementation in Tensorflow 2.0 (Keras).

Dependencies: typing, datetime, pandas, tensorflow, tensorflow_addons, sklearn

Code inspirations:
- https://github.com/titu1994/tf-TabNet/blob/master/tabnet/tabnet.py 

- https://www.kaggle.com/marcusgawronsky/tabnet-in-tensorflow-2-0/execution

### Modifications brought to Marcus Gawronsky implementation:

Global modifications:
 - Modification of the variable names for better understanding
 - Modification of TabNet code global architecture 

Elements taken and modified:

AttentiveTransformer:
 - Different initialization structure

TabNetDecoder :
 - Possibility to choose number of shared and step dependant FC-BN-GLU associations

TabNetEncoder:
 - Better handling of self-supervised learning with priors inititialization
 - Possibility to choose number of shared and step dependant FC-BN-GLU associations
 - Different execution structure
 - Calculation of the features steps importances

TabNetClassifier:
 - Better handling of a pre-trained encoder use
 - Possibility to choose the right activation function according to the classification problem

TabNetAutoencoder:
 - The original code was wrong and incomplete. Thus, it was completed and corrected
 - Correction of the random masks generated
 - Priors initialization of the encoder with the random masks generated
 - Modification of the loss calculation

get_feature:
 - Preprocess function conceived for only one dataframe so it was generalized
 - Modification to handle every types
 - Hash bucket for categorical variables
 - Identity for boolean variables

df_to_dataset:
 - Possibility to encode the target if undone before
 - Handling of datasets for self-supervised learning

## Develop jupyter lab

Build the image by using `sudo make build_tf` in the project dir. start the server with `sudo make start_tf`.

clic the link in terminal (or access it at localhost:8888) to open jupyter lab with the correct access token. shut down the docker with `ctrl+C` when you're done to free some ressources.

when adding new requirements, add them to the fitting `requirements.txt`, rebuild and restart the container to apply the changes.

The project directory is shared with the jupyter lab docker.

## Getting started

There are some preprocess steps to execute before experimenting TabNet. First convert the data types for TensorFlow use with the function `tabnet_utils.data_types` that also returns the types in order to create the dense representation of the feature columns. Obtain the feature columns dense representation with the function `tabnet_utils.get_feature`. Finaly transform the Pandas DataFrames to TensorFlow prefetch datasets with the function `tabnet_utils.df_to_dataset`.

To create a prediction model for classification problems use `TabNetClassifier`. Choose the right activation function according to the classification problem. For a multiclass classification problem choose `classifier_activation=tf.keras.activations.softmax` and for a binary classification problem choose `classifier_activation=tf.keras.activations.sigmoid`.

To create a prediction model for regression problems use `TabNetRegressor`.

The prediction models then propose several methods:
- `fit(...)` to train the models
- `predict(...)` to predict the target
- `explain(...)` to explain the features global importances
- `explain_steps(...)` to explain the features steps importances
- `summary()` to summarize the models

To create an auto-encoder for self-supervised learning use `TabNetAutoencoder`. After training the auto-encoder, recover the pre-trained encoder with  `TabNetAutoencoder.layers[2]` and initialize a prediction model with it: `pretrained_encoder=TabNetAutoencoder.layers[2]`.

For a more complete tutoriel, visit : [notebooks/01-tabnet_tutoriel.ipynb](https://gitlab.analytics.safran/data-services/stages/tabnet-internship/tabnet-implementation/-/blob/master/notebooks/01-tabnet_tutoriel.ipynb)

## Hyper Parameter Tuning

We consider datasets ranging from ∼10K to ∼10M training points, with varying degrees of fitting difficulty. TabNet obtains high performance for all with a few general principles on hyperparameter selection:
- Most datasets yield the best results for Nsteps ∈ [3, 10]. Typically, larger datasets and more complex tasks require a larger Nsteps. A very high value of Nsteps may suffer from overfitting and yield poor generalization.
- Adjustment of the values of Nd and Na is the most efficient way of obtaining a trade-off between performance and complexity. Nd = Na is a reasonable choice for most datasets. A very high value of Nd and Na may suffer from overfitting and yield poor generalization.
- An optimal choice of γ can have a major role on the overall performance. Typically a larger Nsteps value favors for a larger γ.
- A large batch size is beneficial for performance - if the memory constraints permit, as large as 1-10 % of the total training dataset size is suggested.
- Initially large learning rate is important, which should be gradually decayed until convergence.

## Tests coverage

The call and summary methods are overriden and thus TensorFlow compile the code concerned that is then not considered covered by the tests.

Name | Stmts | Miss | Cover
--- | --- | --- | ---
src/read_data.py | 14 | 0 | 100%
src/tabnet/autoencoder.py | 56 | 20 | 64%
src/tabnet/building_blocks.py | 53 | 2 | 96%
src/tabnet/classifier.py | 40 | 13 | 68%
src/tabnet/decoder.py | 22 | 7 | 68%
src/tabnet/encoder.py | 74 | 3 | 96%
src/tabnet/regressor.py | 36 | 10 | 72%
src/tabnet_utils.py | 27 | 1 | 96%
**TOTAL** | **322** | **56**| **83%**
