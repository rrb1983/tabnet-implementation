format:
	@black .

lint:
	@flakehell app

test:
	@pytest

build_tf:
	@docker build -t tabnet-implementation_dev_tf -f tensorflow.Dockerfile .

start_tf:
	@docker run -it --rm --name tensor --mount type=bind,source=$(shell pwd),target=/tf/tabnet-implementation -p 8888:8888 -p 6006:6006 tabnet-implementation_dev_tf 
