#!/bin/sh

tensorboard --host 0.0.0.0 --port 6006 --logdir /tf/logs --debugger_port 6005 &
jupyter lab --ip=0.0.0.0 --allow-root